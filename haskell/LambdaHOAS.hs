{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE CPP #-}

{-# OPTIONS_GHC -Wall #-}

module LambdaHOAS where

import Lib
#define P(t) (Proxy :: Proxy (t))

-- In HOAS, the environment is represented as function arguments
-- Maybe less clear, the one with explicit environment looks
-- more like an actual judgement

class Base lang where
  env :: c v => Proxy c -> lang v
  app :: lang (a -> b) -> (lang a -> lang b)
  lam :: (lang a -> lang b) -> lang (a -> b)

(@@) :: Base lang => lang (a -> b) -> lang a -> lang b
(@@) = app
λ :: Base lang => (lang a -> lang b) -> lang (a -> b)
λ = lam

class Base lang => Let lang where
  let_ :: lang alpha -> (lang alpha -> lang beta) -> lang beta
  let_ e1 e2 = λ e2 @@ e1

data LetList lang alphas beta where
  End   :: lang beta
        -> LetList lang '[] beta
  (:/:) :: lang alpha
        -> (lang alpha -> LetList lang alphas beta)
        -> LetList lang (alpha ': alphas) beta

class Let lang => MultiLet lang where
  multilet :: LetList lang alphas beta -> lang beta
  multilet (End body) = body
  multilet (d :/: ds) = let_ d (multilet . ds)

class Map t
instance t ~ ((a -> b) -> [a] -> [b]) => Map t
class FMap t
instance (Functor f, t ~ ((a -> b) -> f a -> f b)) => FMap t

class Base lang => MapSpec lang where
  map_ :: (alpha ~ (a -> b), beta ~ [a])
       => lang alpha -> lang beta -> lang [b]
  map_ f xs = env P(Map) @@ f @@ xs
class Base lang => FMapLstSpec lang where
  fmap_ :: (alpha ~ (a -> b), beta ~ [a])
        => lang alpha -> lang beta -> lang [b]
  fmap_ f xs = env P(FMap) @@ f @@ xs

-- List comprehensions, from Haskell Report
-- https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-420003.11
data LstCompSyn lang elt where
  -- [e]
  Final :: lang elt
        -> LstCompSyn lang elt
  -- [e | x <- lst]
  Pat   :: lang [alpha]
        -> (lang alpha -> LstCompSyn lang elt)
        -> LstCompSyn lang elt
  -- [e | guard]
  Guard :: lang Bool
        -> LstCompSyn lang elt
        -> LstCompSyn lang elt
  -- [e | let x = expr]
  LetL  :: lang alpha
        -> (lang alpha -> LstCompSyn lang elt)
        -> LstCompSyn lang elt

class Nil t
instance e ~ [a] => Nil e
class Cons t
instance e ~ (a -> [a] -> [a]) => Cons e
class Singleton t
instance e ~ (a -> [a]) => Singleton e
class IfThenElse t
instance e ~ (Bool -> a -> a -> a) => IfThenElse e
class ConcatMap t
instance e ~ ((a -> [b]) -> [a] -> [b]) => ConcatMap e

class Let lang => LstComp lang where
  compr :: LstCompSyn lang elt -> lang [elt]
  compr (Final e)   = env P(Singleton)  @@ e
  compr (Pat p r)   = env P(ConcatMap)  @@ λ (compr . r) @@ p
  compr (Guard g r) = env P(IfThenElse) @@ g @@ compr r @@ env P(Nil)
  compr (LetL b r)  = let_ b (compr . r)
