{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE CPP #-}

{-# OPTIONS_GHC -Wall #-}

module JavaHOAS where

import Lib
#define P(t) (Proxy :: Proxy (t))

#define JType   *
#define JMethod *
#define JSig    ([*], *)

-- JDirectSuperClass + JConstructor + JDirectDefinedMethods
-- defines the class table as in the SoundX paper

-- Define the direct superclass relationship
type family JDirectSuperClass (ty :: JType) :: JType
-- Search for any superclass relation
type family JSuperClass (ty :: JType) (parent :: JType) :: Constraint where
  JSuperClass ty ty    = ()
  JSuperClass ty other = JSuperClass (JDirectSuperClass ty) other

-- Define constructors for class ty
type family JConstructor (ty :: JType) :: [JType]

-- Define methods in class ty
type family JDirectDefinedMethods (ty :: JType) :: [(JMethod, JSig)]
-- Search for a method in any superclass
type family JDefinedMethods (ty :: JType) :: [(JMethod, JSig)] where
  JDefinedMethods () = '[]  -- Stop
  JDefinedMethods ty = JDirectDefinedMethods ty :++: JDefinedMethods (JDirectSuperClass ty)
type family JMethodSig (ty :: JType) (name :: JMethod) :: JSig where
  JMethodSig ty name = Lookup name (JDefinedMethods ty)

-- Check that a type matches a signature
type family Matches (sig :: [JType]) (actual :: [JType]) :: Constraint where
  Matches '[]       '[]       = ()
  Matches (e ': ss) (e ': as) = Matches ss as
  Matches (s ': ss) (a ': as) = (JSuperClass a s, Matches ss as)

data LangElt = VarRef
             | Stmt
             | Expr

class Base (lang :: LangElt   -- type of language element
                 -> JType     -- class in which we are working
                 -> JType     -- return type
                 -> *) where
  -- VARIABLE REFERENCES
  this    :: lang 'VarRef cls cls
  -- STATEMENTS
  -- The first kind of statements are like cons
  -- declaration of a new variable
  varDecl :: Proxy t
          -> (lang 'VarRef cls t -> lang 'Stmt cls r)  -- HOAS
          -> lang 'Stmt cls r
  -- assign an expression to a variable
  assign  :: lang 'VarRef cls v -> lang 'Expr cls v
          -> lang 'Stmt cls r -> lang 'Stmt cls r  -- Continuation-style
  -- while expressions
  while   :: lang 'Expr cls Bool      -- iteration condition
          -> lang 'Stmt cls whatever  -- inside of while
          -> lang 'Stmt cls r -> lang 'Stmt cls r
  -- conditional expressions
  if_     :: lang 'Expr cls Bool
          -> lang 'Stmt cls whatever
          -> lang 'Stmt cls r -> lang 'Stmt cls r
  -- inside brackets, forget the return
  bracket :: lang elt cls whatever
          -> lang 'Stmt cls r
          -> lang 'Stmt cls r
  -- The second kind of statements are like nil
  -- return
  return_ :: lang 'Expr cls r -> lang 'Stmt cls r
  -- end the list of statements, with undefined result
  end     :: lang 'Stmt cls r

  -- EXPRESSIONS
  var     :: lang 'VarRef cls v -> lang 'Expr cls v
  null    :: lang 'Expr cls e     -- `null` is in every class
  call    :: (JMethodSig e method ~ '(params, ret), Matches params ps)
          => lang 'Expr cls e
          -> Proxy method
          -> FVec (lang 'Expr cls) ps
          -> lang 'Expr cls ret
  new     :: (JConstructor t ~ params, Matches params ps)
          => Proxy t -> FVec (lang 'Expr cls) ps
          -> lang 'Expr cls ret

-- Tricky example: a for comprehension
-- Define the iterator type
data Iterator a  -- Generics are directly encoded in Haskell polymorphism :)
type instance JDirectSuperClass (Iterator a) = ()
-- and the corresponding methods
data HasNext
data Next
type instance JDirectDefinedMethods (Iterator a)
  = '( HasNext, '( '[], Bool ) )
  ': '( Next,   '( '[], a) )
  ': '[]
-- We need a last one for getIterator
data GetIterator

class Base lang => For lang where
  for :: forall cls lst a whatever r.
         JMethodSig lst GetIterator ~ '( '[], Iterator a )
      => lang 'Expr cls lst
      -> (lang 'VarRef cls a -> lang 'Stmt cls whatever)
      -> lang 'Stmt cls r -> lang 'Stmt cls r
  for expr inside rest = varDecl P(Iterator a) $ (\v ->
                         assign v (call expr P(GetIterator) FNil) $
                         while (call (var v) P(HasNext) FNil)
                               (varDecl P(a) $ (\x ->
                               assign x (call (var v) P(Next) FNil) $
                               inside x)) $
                         rest)

-- Scala-like for-comprehensions
data ForComprSyn lang cls where
  -- [e]
  Final :: lang 'Stmt cls whatever
        -> ForComprSyn lang cls
  -- [e | x <- lst]
  Pat   :: JMethodSig lst GetIterator ~ '( '[], Iterator a )
        => lang 'Expr cls lst
        -> (lang 'VarRef cls a -> ForComprSyn lang cls)
        -> ForComprSyn lang cls
  -- [e | guard]
  Guard :: lang 'Expr cls Bool
        -> ForComprSyn lang cls
        -> ForComprSyn lang cls

class For lang => ForCompr lang where
  for_ :: ForComprSyn lang cls
       -> lang 'Stmt cls r -> lang 'Stmt cls r
  for_ (Final   st) r = bracket st r
  for_ (Pat e   es) r = for e (\x -> for_ (es x) end) r
  for_ (Guard g es) r = if_ g (for_ es end) r
