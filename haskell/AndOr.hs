{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FunctionalDependencies #-}

module ListCompr where

import Lib

class Base lang where
  var :: Ix i vs ~ v => Proxy i -> lang vs v
  app :: a ~ (b -> r) => lang vs a -> lang vs b -> lang vs r
  lam :: lang (a ': vs) b -> lang vs (a -> b)
  weak :: lang vs b -> lang (a ': vs) b

(@@) :: (Base lang, a ~ (b -> r))
     => lang vs a -> lang vs b -> lang vs r
(@@) = app
λ :: Base lang => lang (a ': vs) b -> lang vs (a -> b)
λ = lam

class Base lang => Let lang where
  let_ :: lang vs alpha -> lang (alpha ': vs) beta -> lang vs beta
  let_ e1 e2 = λ e2 @@ e1

class Base lang => IfThenElse lang where
  ifthenelse :: lang gamma Bool
             -> lang gamma a -> lang gamma a
             -> lang gamma a

class Base lang => HasBool lang where
  true, false :: lang gamma Bool
  and_, or_ :: lang gamma (Bool -> Bool -> Bool)

class (Let lang, IfThenElse lang, HasBool lang) => Or lang where
  orlist :: [lang gamma Bool] -> lang gamma Bool
  orlist []     = true
  orlist [x]    = x
  orlist (x:xs) = let_ x $ ifthenelse (var (Proxy :: Proxy Z))
                                      (var (Proxy :: Proxy Z))
                                      -- (orlist xs)
                                      (weak (orlist xs))

class BaseHOAS lang where
  app_h :: lang (alpha -> beta) -> lang alpha -> lang beta
  lam_h :: (lang alpha -> lang beta) -> lang (alpha -> beta)

class BaseHOAS lang => LetHOAS lang where
  let_h :: lang alpha -> (lang alpha -> lang beta) -> lang beta
  let_h e1 e2 = app_h (lam_h e2) e1

class BaseHOAS lang => BoolHOAS lang where
  true_h, false_h :: lang Bool
  ifthenelse_h :: lang Bool -> lang a -> lang a -> lang a

class (LetHOAS lang, BoolHOAS lang) => OrHOAS lang where
  orlist_h :: [lang Bool] -> lang Bool
  orlist_h []     = true_h
  orlist_h [t]    = t
  orlist_h (t:ts) = let_h t $ \x -> ifthenelse_h x x (orlist_h ts)
