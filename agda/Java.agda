module Java where

  open import Lib

  module Base where
    postulate
      JType   : Set
      Bool    : JType
      Null    : JType

    JEnv : ℕ → Set
    JEnv = Vec JType

    data JParent (child : JType) (parent : JType) : Set where
      jparent : JParent child parent

    data JAscendant : JType → JType → Set where
      me : ∀ {t : JType}
         -- -------------
         → JAscendant t t
      up : ∀ {c p a : JType}
         → JParent c p → JAscendant p a
         -- ---------------------------
         → JAscendant c a

    record JSig : Set where
      constructor signature
      field
        numberofargs : ℕ
        argtys       : Vec JType numberofargs
        returnty     : JType
    open JSig public

    sig : ∀ {n : ℕ} → Vec JType n → JType → JSig
    sig {n} = signature n

    postulate
      -- We postulate methods because they
      -- are distinguished by name
      JMethodDefn : JType → JSig → Set

    record JMethod (cls : JType) (sig : JSig) : Set where
      constructor method
      field
        definedAt  : JType
        relation   : JAscendant cls definedAt
        methodDefn : JMethodDefn definedAt sig

    data matches : {n : ℕ} → Vec JType n → Vec JType n → Set where
      ■    : matches [] []
      _\\_ : ∀ {n} {c p : JType} {r s : Vec JType n}
           → JAscendant c p → matches r s
           -- ------------------------------------------
           → matches (c ∷ r) (p ∷ s)

    matchesme : {n : ℕ} → {v : Vec JType n} → matches v v
    matchesme {z}   {[]}     = ■
    matchesme {s n} {_ ∷ es} = me \\ matchesme

    infixr 6 _,,_

    mutual
      data _,_⊢s_ : {n m : ℕ} → JEnv n → JType → JEnv m → Set where
        varDecl : ∀ {n : ℕ} {env : JEnv n} {cls t : JType}
                -- ---------------------
                → env , cls ⊢s (t ∷ env)
        forgets : ∀ {n : ℕ} {env : JEnv (s n)} {cls : JType}
                → (p : Fin (s n))
                -- -------------------------
                → env , cls ⊢s remove p env

        assign  : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
                → (p : Fin n) → env , cls ⊢e (env [ p ])
                -- ---------------------------------------
                → env , cls ⊢s env
        while   : ∀ {n m : ℕ} {env : JEnv n} {cls : JType} {inner : JEnv m}
                → env , cls ⊢e Bool
                → env , cls ⊢s inner
                -- ------------------
                → env , cls ⊢s env
        if      : ∀ {n m : ℕ} {env : JEnv n} {cls : JType} {inner : JEnv m}
                → env , cls ⊢e Bool
                → env , cls ⊢s inner
                -- ------------------
                → env , cls ⊢s env
        bracket : ∀ {n : ℕ} {env : JEnv n} {cls whatever : JType}
                → env , cls ⊢sℓ whatever
                -- ----------------------
                → env , cls ⊢s env

      data _,_⊢sℓ_ : {n : ℕ} → JEnv n → JType → JType → Set where
        _,,_    : ∀ {n m : ℕ} {env : JEnv n} {env' : JEnv m} {cls r : JType}
                → env  , cls ⊢s env'
                → env' , cls ⊢sℓ r
                -- -----------------
                → env , cls ⊢sℓ r
        return  : ∀ {n : ℕ} {env : JEnv n} {cls r : JType}
                → env , cls ⊢e r
                -- ---------------
                → env , cls ⊢sℓ r
        end     : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
                -- ---------------------------------------
                → env , cls ⊢sℓ Null

      data _,_⊢e_ : {n : ℕ} → JEnv n → JType → JType → Set where
        var  : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
             → (p : Fin n)
             -- --------------------------------------
             → env , cls ⊢e (env [ p ])
        this : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
             -- --------------------------------------
             → env , cls ⊢e cls
        null : ∀ {n : ℕ} {env : JEnv n} {cls r : JType}
             -- ----------------------------------------
             → env , cls ⊢e r
        call : ∀ {n : ℕ} {env : JEnv n} {cls x : JType}
                 {s : JSig} {xs : Vec JType (numberofargs s)}
             → env , cls ⊢e x → JMethod x s → env , cls ⊢eℓ xs
             → matches (argtys s) xs
             -- ---------------------------------------------------
             → env , cls ⊢e (returnty s)

        forgete : ∀ {n : ℕ} {env : JEnv (s n)} {cls r : JType}
                → (p : Fin (s n)) → remove p env , cls ⊢e r
                -- -----------------------------------------
                → env , cls ⊢e r

      data _,_⊢eℓ_ {n : ℕ} (env : JEnv n) (cls : JType) : {m : ℕ} → Vec JType m → Set where
        instance []'  : env , cls ⊢eℓ []
        instance _∷'_ : ∀ {m} {r : JType} {rs : Vec JType m}
                      → env , cls ⊢e r → env , cls ⊢eℓ rs
                      → env , cls ⊢eℓ (r ∷ rs)

    call' : ∀ {n : ℕ} {env : JEnv n} {cls x : JType} {s : JSig}
          → env , cls ⊢e x → JMethodDefn x s → env , cls ⊢eℓ (argtys s)
          → env , cls ⊢e (returnty s)
    call' expr m args = call expr (method _ me m) args matchesme

  module Plus where
    open Base public

    postulate
      Int : JType
      Add : JMethodDefn Int (sig (_∷_ Int []) Int)

    plus_ : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
          → env , cls ⊢e Int → env , cls ⊢e Int
          → env , cls ⊢e Int
    plus_ x y = call x (method Int me Add) (y ∷' []') (me \\ ■)


  module For where
    open Base public

    postulate
      Iterator : JType → JType
      HasNext : ∀ {a} → JMethodDefn (Iterator a) (sig [] Bool)
      Next    : ∀ {a} → JMethodDefn (Iterator a) (sig [] a)

    for : ∀ {n m : ℕ} {env : JEnv n} {inner : JEnv m} {cls lst elt : JType}
        → JMethod lst (sig [] (Iterator elt))
        → env , cls ⊢e lst
        → (elt ∷ env) , cls ⊢s inner
        -- --------------------------
        → env , cls ⊢sℓ Null
    for {_} {_} {_} {_} {_} {_} {elt} GetIterator expr inside
      =    varDecl
      ,, assign fz (call (forgete fz expr) GetIterator []' ■)
      ,, while (call' (var fz) HasNext []')
               (bracket (varDecl
                     ,, assign fz (call' (var (fs fz)) Next []')
                     ,, forgets (fs fz)
                     ,, inside
                     ,, end))
      ,, end

  module ForCompr where
    open For public

    data ForCompr : {n : ℕ} → JEnv n → JType → Set where
      e : ∀ {n m : ℕ} {env : JEnv n} {inner : JEnv m} {cls : JType}
        → env , cls ⊢s inner
        -- -----------------
        → ForCompr env cls
      p : ∀ {n : ℕ} {env : JEnv n} {cls lst elt : JType}
        → JMethod lst (sig [] (Iterator elt))
        → env , cls ⊢e lst
        → ForCompr (elt ∷ env) cls
        -- -----------------------
        → ForCompr env cls
      g : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
        → env , cls ⊢e Bool
        → ForCompr env cls
        -- -----------------------
        → ForCompr env cls

    forc : ∀ {n : ℕ} {env : JEnv n} {cls : JType}
         → ForCompr env cls
         → env , cls ⊢sℓ Null
    forc (e expr)        = expr ,, end
    forc (p m expr rest) = for m expr (bracket (forc rest))
    forc (g cond rest)   = if cond (bracket (forc rest)) ,, end
