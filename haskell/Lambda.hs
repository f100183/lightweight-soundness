{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
-- For FMap
{-# LANGUAGE UndecidableInstances #-}

{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -Wall #-}

#define P(t) (Proxy :: Proxy (t))

module Lambda where

import Control.Applicative
import Lib

-- Finally-tagless approach
-- Ideas from Roman Cheplyaka's blog post
-- about Oleg Kiselyov's LINQ paper

-- Base language

class Base lang where
  env :: c v => Proxy c -> lang vs v
  var :: Ix i vs ~ v => Proxy i -> lang vs v
  app :: a ~ (b -> r) => lang vs a -> lang vs b -> lang vs r
  lam :: lang (a ': vs) b -> lang vs (a -> b)

(@@) :: (Base lang, a ~ (b -> r))
     => lang vs a -> lang vs b -> lang vs r
(@@) = app
λ :: Base lang => lang (a ': vs) b -> lang vs (a -> b)
λ = lam
-- Just to get a better way to deal with environment
env_ :: forall c lang vs v. (Base lang, c v) => lang vs v
env_ = env (Proxy :: Proxy c)
-- ... and the variables
var_ :: forall i lang vs v. (Base lang, Ix i vs ~ v) => lang vs v
var_ = var (Proxy :: Proxy i)

-- Let bindings and multiple lets in terms of those others
class Base lang => Let lang where
          -- Tell me the typing rule here
  let_ :: lang vs alpha -> lang (alpha ': vs) beta -> lang vs beta
          -- Tell me the translation here
  let_ e1 e2 = λ e2 @@ e1

data LetList lang vs alphas beta where
  End   :: lang vs beta
        -> LetList lang vs '[] beta
  (:/:) :: lang vs alpha
        -> LetList lang (alpha ': vs) alphas beta
        -> LetList lang vs (alpha ': alphas) beta

class Let lang => MultiLet lang where
  multilet :: LetList lang vs alphas beta -> lang vs beta
  multilet (End body) = body
  multilet (d :/: ds) = let_ d (multilet ds)

class Let lang => MultiLet2 lang where
  end  :: lang vs beta -> lang vs beta
  end body = body
  (//) :: lang vs alpha -> lang (alpha ': vs) beta
       -> lang vs beta
  d // ds = let_ d ds

-- We use a type class because the following is not accepted
-- type Map t = t ~ ((a -> b) -> [a] -> [b])
class Map t
instance t ~ ((a -> b) -> [a] -> [b]) => Map t

class Base lang => MapSpec lang where
  map_ :: (alpha ~ (a -> b), beta ~ [a])
       => lang vs alpha  ->  lang vs beta
       ----------------------------------
       -> lang vs [b]
  map_ f xs = env_ @Map @@ f @@ xs

class Base lang => HasMap lang where
  mapfn :: lang vs ((a -> b) -> [a] -> [b])

class HasMap lang => MapSpec2 lang where
  map__ :: (tau ~ (alpha -> beta), rho ~ [delta], alpha ~ delta)
        => lang gamma tau -> lang gamma rho
        -> lang gamma [beta]
  map__ fn lst = mapfn @@ fn @@ lst

data MapSpecContext gamma tau where
  FoundMap   :: MapSpecContext gamma ((alpha -> beta) -> [alpha] -> [beta])
  FoundMapFn :: (forall m. MapSpec2 m => m gamma (alpha -> beta)) -> MapSpecContext gamma ([alpha] -> [beta])
  NotFound   :: MapSpecContext gamma tau

data MSC gamma tau  = MSC (MapSpecContext gamma tau) (forall m. MapSpec2 m => m gamma tau)

instance Base MSC where
  var v = MSC NotFound (var v)
  app (MSC FoundMap m) (MSC _ fn)
    = MSC (FoundMapFn fn) (app m fn)
  app (MSC (FoundMapFn fn) mfn) (MSC _ lst)
    = MSC NotFound (map__ fn lst)
  app (MSC NotFound f) (MSC _ e)
    = MSC NotFound (app f e)
  lam (MSC _ e) = MSC NotFound (lam e)

instance HasMap MSC where
  mapfn = MSC FoundMap mapfn

unmap__ :: (forall b. HasMap b => b gamma tau) -> (forall m. MapSpec2 m => m gamma tau)
unmap__ = unmap' where unmap' (MSC _ e) = e

class FMap t
instance (Functor f, t ~ ((a -> b) -> f a -> f b)) => FMap t

class Base lang => FMapLstSpec lang where
  fmap_ :: (alpha ~ (a -> b), beta ~ [a])
        => lang vs alpha -> lang vs beta -> lang vs [b]
  fmap_ f xs = env_ @FMap @@ f @@ xs

class Base lang => DifferentEnv lang where
  kno :: Proxy t -> lang gamma t

fmapfn :: (DifferentEnv lang, Functor f) => lang gamma ((a -> b) -> f a -> f b)
fmapfn = kno Proxy

class DifferentEnv lang => FMapLstSpec2 lang where
  fmap__ :: (alpha ~ (a -> b), beta ~ [a])
         => lang vs alpha -> lang vs beta -> lang vs [b]
  fmap__ f xs = fmapfn @@ f @@ xs

-- List literals
class Nil t
instance e ~ [a] => Nil e

class Cons t
instance e ~ (a -> [a] -> [a]) => Cons e

data LstSyn elt lang (vs :: [*]) where
  LstNil  :: LstSyn elt lang vs
  LstCons :: lang vs elt -> LstSyn elt lang vs -> LstSyn elt lang vs

class Base lang => Lst lang where
  lst :: LstSyn elt lang vs -> lang vs [elt]
  lst LstNil         = env_ @Nil
  lst (LstCons x xs) = env_ @Cons @@ x @@ lst xs

-- List comprehensions, from Haskell Report
-- https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-420003.11
data LstCompSyn elt lang vs where
  -- [e]
  Final :: lang vs elt
        -> LstCompSyn elt lang vs
  -- [e | x <- lst]
  Pat   :: lang vs [alpha]
        -> LstCompSyn elt lang (alpha ': vs)
        -> LstCompSyn elt lang vs
  -- [e | guard]
  Guard :: lang vs Bool
        -> LstCompSyn elt lang vs
        -> LstCompSyn elt lang vs
  -- [e | let x = expr]
  LetL  :: lang vs alpha
        -> LstCompSyn elt lang (alpha ': vs)
        -> LstCompSyn elt lang vs

class Singleton t
instance e ~ (a -> [a]) => Singleton e

class IfThenElse t
instance e ~ (Bool -> a -> a -> a) => IfThenElse e

class ConcatMap t
instance e ~ ((a -> [b]) -> [a] -> [b]) => ConcatMap e

class Let lang => LstComp lang where
  compr :: LstCompSyn elt lang vs -> lang vs [elt]
  compr (Final e)   = env_ @Singleton  @@ e
  compr (Pat p r)   = env_ @ConcatMap  @@ λ (compr r) @@ p
  compr (Guard g r) = env_ @IfThenElse @@ g @@ compr r @@ env_ @Nil
  compr (LetL b r)  = let_ b (compr r)

class Base lang => HasList lang where
  nilfn  :: lang gamma [a]
  consfn :: lang gamma (a -> [a] -> [a])
class Base lang => HasConcatMap lang where
  concatmapfn :: lang gamma ((a -> [b]) -> [a] -> [b])
class Base lang => IfThenElse2 lang where
  ifthenelse :: lang gamma Bool
             -> lang gamma a -> lang gamma a
             -> lang gamma a

class (Let lang, HasList lang, HasConcatMap lang, IfThenElse2 lang) => LstComp2 lang where
  compr2 :: LstCompSyn elt lang vs -> lang vs [elt]
  compr2 (Final e)   = consfn @@ e @@ nilfn
  compr2 (Pat p r)   = concatmapfn @@ λ (compr2 r) @@ p
  compr2 (Guard g r) = ifthenelse g (compr2 r) nilfn
  compr2 (LetL b r)  = let_ b (compr2 r)

class LstCompFinal lang comp where
  final :: lang vs elt                               -> comp vs elt
  pat   :: lang vs [alpha] -> comp (alpha ': vs) elt -> comp vs elt
  guard :: lang vs Bool    -> comp vs            elt -> comp vs elt
  letc  :: lang vs alpha   -> comp (alpha ': vs) elt -> comp vs elt

data LangLst lang vs elt = LangLst { unLangLst :: lang vs [elt] }

instance ( HasList lang, IfThenElse2 lang, Let lang, HasConcatMap lang )
         => LstCompFinal lang (LangLst lang) where
  final   e = LangLst $ consfn @@ e @@ nilfn
  pat   p r = LangLst $ concatmapfn @@ λ (unLangLst r) @@ p
  guard g r = LangLst $ ifthenelse g (unLangLst r) nilfn
  letc  b r = LangLst $ let_ b (unLangLst r)

compr3 :: LangLst lang vs elt -> lang vs [elt]
compr3 = unLangLst

-- To prove completeness, we need a way to recognize functions
-- We do so by enlarging the language with special constants

-- In this case, we are doing so with `map`, `nil`, `cons` and `fmap`
-- Note: we do not use the InstanceOfMap type class,
-- because it inhibits improvement

class Base lang => KnownFns fn lang where
  known :: fn v -> lang vs v

data KnownFnsFree fn vs v where
  Env :: c v => Proxy c -> KnownFnsFree fn vs v
  Var :: Ix i vs ~ v => Proxy i -> KnownFnsFree fn vs v
  App :: a ~ (b -> r)
      => KnownFnsFree fn vs a -> KnownFnsFree fn vs b
      -> KnownFnsFree fn vs r
  Lam :: KnownFnsFree fn (a ': vs) b
      -> KnownFnsFree fn vs (a -> b)
  Kno :: fn v -> KnownFnsFree fn vs v

instance Base (KnownFnsFree fn) where
  env = Env
  var = Var
  app = App
  lam = Lam
instance KnownFns fn (KnownFnsFree fn) where
  known = Kno

data RecognizeMap r where
  DeMap :: RecognizeMap ((a -> b) -> [a] -> [b])
data RecognizeFMap r where
  DeFMap :: Functor f => RecognizeFMap ((a -> b) -> f a -> f b)
data RecognizeLst r where
  DeNil  :: RecognizeLst [e]
  DeCons :: RecognizeLst (e -> [e] -> [e])

unmap :: (forall b. KnownFns RecognizeMap b => b vs t)
      -> (forall m. MapSpec m => m vs t)
unmap x = unmap_ x
  where unmap_ :: KnownFnsFree RecognizeMap vs t -> (forall m. MapSpec m => m vs t)
        unmap_ (App (App (Kno DeMap) f) xs) = map_ (unmap_ f) (unmap_ xs)
        unmap_ (Env s)     = env s
        unmap_ (Var i)     = var i
        unmap_ (App e1 e2) = app (unmap_ e1) (unmap_ e2)
        unmap_ (Lam b)     = lam (unmap_ b)
        unmap_ (Kno DeMap) = env_ @FMap

unfmap :: (forall b. KnownFns RecognizeFMap b => b vs t)
       -> (forall m. FMapLstSpec m => m vs t)
unfmap x = unfmap_ x
  where unfmap_ :: KnownFnsFree RecognizeFMap vs t -> (forall m. FMapLstSpec m => m vs t)
        -- This line fails because it is not complete
        -- unfmap_ (App (App (Kno DeFMap) f) xs) = fmap_ (unfmap_ f) (unfmap_ xs)
        -- ... but if we remove the line, the fn does not perform translation
        unfmap_ (Env s)      = env s
        unfmap_ (Var i)      = var i
        unfmap_ (App e1 e2)  = app (unfmap_ e1) (unfmap_ e2)
        unfmap_ (Lam b)      = lam (unfmap_ b)
        unfmap_ (Kno DeFMap) = env_ @FMap

unlst :: (forall b. KnownFns RecognizeLst b => b vs t)
      -> (forall m. Lst m => m vs t)
unlst = unlst_
  where unlst_ :: Lst m => KnownFnsFree RecognizeLst vs t -> m vs t
        unlst_ e@(Kno DeNil)
          | let Just syn = unlstSyn e = lst syn
        unlst_ e@(App (App (Kno DeCons) _) _)
          | Just syn <- unlstSyn e    = lst syn
        unlst_ (Kno DeCons)           = env_ @Cons
        unlst_ (Env s)                = env s
        unlst_ (Var i)                = var i
        unlst_ (App e1 e2)            = app (unlst_ e1) (unlst_ e2)
        unlst_ (Lam b)                = lam (unlst_ b)
        unlstSyn :: Lst m => KnownFnsFree RecognizeLst vs [b] -> Maybe (LstSyn b m vs)
        unlstSyn (Kno DeNil) = pure LstNil
        unlstSyn (App (App (Kno DeCons) x) xs) = LstCons (unlst_ x) <$> unlstSyn xs
        unlstSyn _     = empty

{-
class Base lang => Weaken lang where
  weaken :: Remove i vs ~ vs'
         => Proxy i -> lang vs' r  -- Forget variable i
         -> lang vs r

data RecognizeAppendComp r where
  DeAppend  :: RecognizeAppendComp ([a] -> [a] -> [a])
  DeCompose :: RecognizeAppendComp ((b -> c) -> (a -> b) -> a -> c)

diff_style :: (forall b. KnownFns RecognizeAppendComp b => b vs t)
           -> (forall b. KnownFns RecognizeAppendComp b => b vs t)
diff_style = diff_style_
  where diff_style_ :: KnownFnsFree RecognizeAppendComp vs t
                    -> (forall b. KnownFns RecognizeAppendComp b => b vs t)
        diff_style_ (App (App (Kno DeAppend) x) y)
          = known DeCompose @@ λ (known DeAppend @@ weaken P(Z) (diff_style_ x) @@ var P(Z))
                            @@ λ (known DeAppend @@ weaken P(Z) (diff_style_ y) @@ var P(Z))
-}
