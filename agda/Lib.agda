module Lib where

  open import Agda.Primitive public
  -- open import Agda.Builtin.Equality public

  data _≡_ {A : Set} : A → A → Set where
    refl : ∀ {x : A} → x ≡ x

  data ixeq {a b} {A : Set a} {B : A → Set b} : (x₁ x₂ : A) → B x₁ → B x₂ → Set (a ⊔ b) where
    reflrefl : ∀ {x : A} {y : B x} → ixeq x x y y

  data ℕ : Set where
    z : ℕ
    s : ℕ → ℕ

  data Fin : ℕ → Set where
    fz : ∀ {n} → Fin (s n)
    fs : ∀ {n} → Fin n → Fin (s n)

  data Vec (Ty : Set) : ℕ → Set where
    []  : Vec Ty z
    _∷_ : {n : ℕ} → Ty → Vec Ty n → Vec Ty (s n)

  _[_] : ∀ {Ty n} → Vec Ty n → Fin n → Ty
  (x ∷ _ ) [ fz ]   = x
  (_ ∷ xs) [ fs r ] = xs [ r ]

  remove : ∀ {Ty} → {n : ℕ} → Fin (s n) → Vec Ty (s n) → Vec Ty n
  remove fz      (x ∷ xs)       = xs
  remove (fs ()) (x ∷ [])
  remove (fs i)  (x ∷ (y ∷ ys)) = x ∷ remove i (y ∷ ys)

  repeat : ∀ {e} → e → (n : ℕ) → Vec e n
  repeat _ z     = []
  repeat x (s i) = x ∷ repeat x i

  infixr 4 _,_

  record Σ {a b} (A : Set a) (B : A → Set b) : Set (a ⊔ b) where
    constructor _,_
    field
      projl : A
      projr : B projl

  _×_ : ∀ {a b} (A : Set a) (B : Set b) → Set (a ⊔ b)
  A × B = Σ A (λ _ → B)

  data Maybe (A : Set) : Set where
    Just : A → Maybe A
    Nothing :  Maybe A

  data ⊤ : Set where
    tt : ⊤

  data ⊥ : Set where
