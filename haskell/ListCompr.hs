{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FunctionalDependencies #-}

module ListCompr where

import Lib

class Base lang where
  var :: Ix i vs ~ v => Proxy i -> lang vs v
  app :: a ~ (b -> r) => lang vs a -> lang vs b -> lang vs r
  lam :: lang (a ': vs) b -> lang vs (a -> b)

(@@) :: (Base lang, a ~ (b -> r))
     => lang vs a -> lang vs b -> lang vs r
(@@) = app
λ :: Base lang => lang (a ': vs) b -> lang vs (a -> b)
λ = lam

class Base lang => Let lang where
  let_ :: lang vs alpha -> lang (alpha ': vs) beta -> lang vs beta
  let_ e1 e2 = λ e2 @@ e1

data Qualifier lang gamma delta where
  Pat   :: lang gamma [alpha] -> Qualifier lang gamma (alpha ': gamma)
  Guard :: lang gamma Bool    -> Qualifier lang gamma gamma
  LetL  :: lang gamma alpha   -> Qualifier lang gamma (alpha ': gamma)

data QualifierList lang gamma delta where
  QuNil  :: QualifierList lang gamma gamma
  QuCons :: Qualifier lang gamma delta -> QualifierList lang delta sigma -> QualifierList lang gamma sigma

class Base lang => HasList lang where
  nilfn  :: lang gamma [a]
  consfn :: lang gamma (a -> [a] -> [a])
class Base lang => HasConcatMap lang where
  concatmapfn :: lang gamma ((a -> [b]) -> [a] -> [b])
class Base lang => IfThenElse lang where
  ifthenelse :: lang gamma Bool
             -> lang gamma a -> lang gamma a
             -> lang gamma a

class (Let lang, HasList lang, HasConcatMap lang, IfThenElse lang) => ListCompr lang where
  compr :: QualifierList lang gamma delta -> lang delta elt -> lang gamma [elt]
  compr QuNil                e = consfn @@ e @@ nilfn
  compr (Pat p   `QuCons` r) e = concatmapfn @@ lam (compr r e) @@ p
  compr (Guard g `QuCons` r) e = ifthenelse g (compr r e) nilfn
  compr (LetL l  `QuCons` r) e = let_ l (compr r e)

class Q lang qual | qual -> lang where
  pat   :: lang gamma [alpha] -> qual gamma (alpha ': gamma)
  guard :: lang gamma Bool    -> qual gamma gamma
  -- letl  :: lang gamma alpha   -> qual gamma (alpha ': gamma)

data QList qual gamma delta where
  QNil  :: QList qual gamma gamma
  QCons :: qual gamma delta
        -> QList qual delta sigma
        -> QList qual gamma sigma

data LangLst lang elt gamma delta = LangLst { unLangLst :: lang delta [elt] -> lang gamma [elt] }

instance (HasList lang, HasConcatMap lang, IfThenElse lang) => Q lang (LangLst lang elt) where
  pat   p = LangLst $ \rest -> concatmapfn @@ lam rest @@ p
  guard g = LangLst $ \rest -> ifthenelse g rest nilfn
  -- letl  b = LangLst $ \rest -> let_ b rest

compr2 :: (HasList lang, HasConcatMap lang, IfThenElse lang)
       => QList (LangLst lang elt) gamma delta -> lang delta elt -> lang gamma [elt]
compr2 QNil        e = consfn @@ e @@ nilfn
compr2 (QCons q r) e = unLangLst q (compr2 r e)

example :: (Base lang, IfThenElse lang, HasConcatMap lang, HasList lang) => lang '[[Int]] [Int]
example = compr2 (pat (var (Proxy :: Proxy Z)) `QCons` QNil) (var (Proxy :: Proxy Z))

class HasList lang => ListCompr3 lang where
  compr3 :: QList (LangLst lang elt) gamma delta -> lang delta elt -> lang gamma [elt]
  compr3 QNil        e = consfn @@ e @@ nilfn
  compr3 (QCons q r) e = unLangLst q (compr3 r e)

class Q lang qual => QLet lang qual where
  letl :: lang gamma alpha -> qual gamma (alpha ': gamma)

instance (HasList lang, HasConcatMap lang, IfThenElse lang, Let lang) => QLet lang (LangLst lang elt) where
  letl b = LangLst $ \rest -> let_ b rest

example2 :: (Base lang, IfThenElse lang, HasConcatMap lang, HasList lang, Let lang) => lang '[[Int]] [Int]
example2 = compr2 ( pat (var (Proxy :: Proxy Z))
                    `QCons` (letl (var (Proxy :: Proxy Z))
                    `QCons` QNil) )
                  ( var (Proxy :: Proxy Z) )
