{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE TypeInType #-}

{-# OPTIONS_GHC -Wall #-}

module Java where

import Data.Kind
import Lib
#define P(t) (Proxy :: Proxy (t))

-- Type (w/o generic arguments for now)
type JType   = *
type JMethod = *
type JSig    = ([JType], JType)
-- #define JType   *
-- #define JMethod *
-- #define JSig    ([*], *)

-- JDirectSuperClass + JConstructor + JDirectDefinedMethods
-- defines the class table as in the SoundX paper

-- Define the direct superclass relationship
type family JDirectSuperClass (ty :: JType) :: JType
-- Search for any superclass relation
type family JSuperClass (ty :: JType) (parent :: JType) :: Constraint where
  JSuperClass ty ty    = ()
  JSuperClass ty other = JSuperClass (JDirectSuperClass ty) other

-- Define constructors for class ty
type family JConstructor (ty :: JType) :: [JType]

-- Define methods in class ty
type family JDirectDefinedMethods (ty :: JType) :: [(JMethod, JSig)]
-- Search for a method in any superclass
type family JDefinedMethods (ty :: JType) :: [(JMethod, JSig)] where
  JDefinedMethods () = '[]  -- Stop
  JDefinedMethods ty = JDirectDefinedMethods ty :++: JDefinedMethods (JDirectSuperClass ty)
type family JMethodSig (ty :: JType) (name :: JMethod) :: JSig where
  JMethodSig ty name = Lookup name (JDefinedMethods ty)

-- Check that a type matches a signature
type family Matches (sig :: [JType]) (actual :: [JType]) :: Constraint where
  Matches '[]       '[]       = ()
  Matches (e ': ss) (e ': as) = Matches ss as
  Matches (s ': ss) (a ': as) = (JSuperClass a s, Matches ss as)

data LangElt = Stmt
             | Expr

class Base (lang :: LangElt   -- type of language element
                 -> JType     -- class in which we are working
                 -> [JType]   -- environment
                 -> JType     -- return type
                 -> *) where
  -- STATEMENTS
  -- The first kind of statements are like cons
  -- declaration of a new variable
  varDecl :: Proxy t -> lang 'Stmt cls (t ': vs) r -> lang 'Stmt cls vs r
  -- assign an expression to a variable
  assign  :: Ix i vs ~ v => Proxy i -> lang 'Expr cls vs v
          -> lang 'Stmt cls vs r -> lang 'Stmt cls vs r
  -- while expressions
  while   :: lang 'Expr cls vs Bool      -- iteration condition
          -> lang 'Stmt cls vs whatever  -- inside of while
          -> lang 'Stmt cls vs r -> lang 'Stmt cls vs r
  -- conditional expressions
  if_     :: lang 'Expr cls vs Bool
          -> lang 'Stmt cls vs whatever
          -> lang 'Stmt cls vs r -> lang 'Stmt cls vs r
  -- inside brackets, forget the return
  bracket :: lang elt cls vs whatever
          -> lang 'Stmt cls vs r
          -> lang 'Stmt cls vs r
  -- The second kind of statements are like nil
  -- return
  return_ :: lang 'Expr cls vs r -> lang 'Stmt cls vs r
  -- end the list of statements, with undefined result
  end     :: lang 'Stmt cls vs r

  -- EXPRESSIONS
  var     :: Ix i vs ~ v => Proxy i -> lang 'Expr cls vs v
  this    :: lang 'Expr cls vs cls
  null    :: lang 'Expr cls vs e     -- `null` is in every class
  call    :: (JMethodSig e method ~ '(params, ret), Matches params ps)
          => lang 'Expr cls vs e
          -> Proxy method
          -> FVec (lang 'Expr cls vs) ps
          -> lang 'Expr cls vs ret
{-
  new     :: (JConstructor t ~ params, Matches params ps)
          => Proxy t -> FVec (lang 'Expr cls vs) ps
          -> lang 'Expr cls vs ret
-}

  -- FORGET VARIABLES
  forget  :: Remove i vs ~ vs'
          => Proxy i -> lang elt cls vs' r  -- Forget variable i
          -> lang elt cls vs r

-- Example 1: a + operation
-- Define the Int type
type instance JDirectSuperClass Int = ()
-- Define its Add method
data Add
type instance JDirectDefinedMethods Int = '(Add, '( '[Int], Int )) ': '[]

class Base lang => Plus lang where
  plus :: lang 'Expr cls vs Int -> lang 'Expr cls vs Int -> lang 'Expr cls vs Int
  plus x y = call x P(Add) (FCons y FNil)

-- Example 2: a for comprehension
-- Define the iterator type
data Iterator a  -- Generics are directly encoded in Haskell polymorphism :)
type instance JDirectSuperClass (Iterator a) = ()
-- and the corresponding methods
data HasNext
data Next
type instance JDirectDefinedMethods (Iterator a)
  = '( HasNext, '( '[], Bool ) )
  ': '( Next,    '( '[], a) )
  ': '[]
-- We need a last one for getIterator
data GetIterator

class Base lang => For lang where
  for :: forall cls vs lst a whatever r.
         JMethodSig lst GetIterator ~ '( '[], Iterator a )
      => lang 'Expr cls vs lst
      -> lang 'Stmt cls (a ': vs) whatever
      -> lang 'Stmt cls vs r -> lang 'Stmt cls vs r
  for expr inside rest = varDecl P(Iterator a) $
                         assign P(Z) (call (forget P(Z) expr) P(GetIterator) FNil) $
                         while (call (var P(Z)) P(HasNext) FNil)
                               (varDecl P(a) $
                                assign P(Z) (call (var P(S Z)) P(Next) FNil) $
                                forget P(S Z) inside) $
                         forget P(Z) rest

-- Scala-like for-comprehensions
data ForComprSyn lang cls vs where
  -- [e]
  Final :: lang 'Stmt cls vs whatever
        -> ForComprSyn lang cls vs
  -- [e | x <- lst]
  Pat   :: JMethodSig lst GetIterator ~ '( '[], Iterator a )
        => lang 'Expr cls vs lst
        -> ForComprSyn lang cls (a ': vs)
        -> ForComprSyn lang cls vs
  -- [e | guard]
  Guard :: lang 'Expr cls vs Bool
        -> ForComprSyn lang cls vs
        -> ForComprSyn lang cls vs

class For lang => ForCompr lang where
  for_ :: ForComprSyn lang cls vs
       -> lang 'Stmt cls vs r -> lang 'Stmt cls vs r
  for_ (Final   st) r = bracket st r
  for_ (Pat e   es) r = for e (for_ es end) r
  for_ (Guard g es) r = if_ g (for_ es end) r
