{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
-- For Uppend
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wall #-}

-- Scheme expressions
-- Based on:
-- * http://www.cs.sfu.ca/CourseCentral/383/havens/notes/Lecture05.pdf
-- * http://matt.might.net/articles/desugaring-scheme/
module Scheme where

import Lib
#define P(t) (Proxy :: Proxy (t))

-- Language definition

-- Forms in the syntax
data LangElt = VarRef | Expr
-- To build function types
type family Function args r where
  Function '[]       r = r
  Function (x ': xs) r = x -> Function xs r

class Base (lang :: LangElt -> [*] -> * -> *) where
  -- Reference to variables
  e :: Proxy t -> lang 'VarRef vs t
  v :: Ix i vs ~ v => SNat i -> lang 'VarRef vs v

  -- Expressions
  var   :: lang 'VarRef vs v
        -> lang 'Expr vs v
  set   :: lang 'VarRef vs v
        -> lang 'Expr vs v
        -> lang 'Expr vs ()
  -- Cannot have multi-lambda because of the restriction on :--:
  lam   :: lang 'Expr (a ': vs) b
        -> lang 'Expr vs (a -> b)
  app   :: lang 'Expr vs (Function as b)
        -> FVec (lang 'Expr vs) as
        -> lang 'Expr vs b
  begin :: FNotVec (lang 'Expr vs) -- Multi-line expression,
        -> lang 'Expr vs r         -- the returned type is the last
        -> lang 'Expr vs r
  if_   :: lang 'Expr vs Bool
        -> lang 'Expr vs r
        -> lang 'Expr vs r
        -> lang 'Expr vs r

  -- Weaken the environment
  weaken :: lang elt vs r -> lang elt (v ': vs) r

-- Abbreviations
(@@) :: Base lang
     => lang 'Expr vs (Function as b)
     -> FVec (lang 'Expr vs) as
     -> lang 'Expr vs b
(@@) = app
λ :: Base lang
  => lang 'Expr (a ': vs) b
  -> lang 'Expr vs (a -> b)
λ = lam
-- Some constants
void :: Base lang => lang 'Expr vs a
void  = var (e P(a))
true, false :: Base lang => lang 'Expr vs Bool
true  = var (e P(Bool))
false = var (e P(Bool))

-- Let bindings and multiple lets in terms of those others
class Base lang => Let lang where
          -- Tell me the typing rule here
  let_ :: lang 'Expr vs alpha
       -> lang 'Expr (alpha ': vs) beta
       -> lang 'Expr vs beta
          -- Tell me the translation here
  let_ e1 e2 = λ e2 @@ (FCons e1 FNil)

-- Now lec-rec bindings
class Let lang => LetRec lang where
  letrec :: lang 'Expr (alpha ': vs) alpha
         -> lang 'Expr (alpha ': vs) beta
         -> lang 'Expr vs beta
  letrec e1 e2 = let_ void $ begin (FNotCons (set (v ZZ) e1)
                                    FNotNil)
                                   e2

{-
-- CAN'T MAKE IT WORK :(
-- It seems that we need injectivity on type families
-- And now multiple let-rec
type family Uppend xs ys where
  Uppend '[]       ys = ys
  Uppend (x ': xs) ys = Uppend xs (x ': ys)

data LetRecSyn lang vs n alphas where
  LetRecEnd :: LetRecSyn lang vs Z '[]
  LetRecOne :: Elem i vs ~ v
            => LetRecSyn lang vs i alphas
            -> lang Expr vs alpha
            -> LetRecSyn lang vs (S i) (alpha ': alphas)

class Let lang => LetRec lang where
  letrec_ :: LetRecSyn lang (Uppend alphas vs) n alphas
          -> lang Expr (Uppend alphas vs) beta
          -> lang Expr vs beta
  letrec_ es b = letrec1 es b
  -- letrec_ LetRecEnd        b = b
  -- letrec_ (LetRecOne es e) b = let_ void (letrec_ es b)
    where letrec1 :: LetRecSyn lang (Uppend alphas vs) n gammas
                  -> lang Expr (Uppend alphas vs) beta
                  -> lang Expr vs beta
          letrec1 LetRecEnd        b = b
          letrec1 (LetRecOne es e) b = let_ void (letrec_ es b)
-}

-- Conditionals
data CondSyn lang (vs :: [*]) r where
  -- End without else
  CondEnd  :: CondSyn lang vs ()
  -- End with else
  CondElse :: lang 'Expr vs r -> CondSyn lang vs r
  -- Add condition
  CondIf   :: (lang 'Expr vs Bool, lang 'Expr vs r)
           -> CondSyn lang vs r  -- The rest
           -> CondSyn lang vs r

class Base lang => Cond lang where
  cond :: CondSyn lang vs r -> lang 'Expr vs r
  cond CondEnd                = void
  cond (CondElse r)           = r
  cond (CondIf (prd, yes) no) = if_ prd yes (cond no)

class Let lang => AndOr lang where
  -- Scheme returns the last true value
  and_ :: [lang 'Expr vs Bool] -> lang 'Expr vs Bool
  and_ []     = true
  -- We need to be careful in this to not repeat set's
  and_ (x:xs) = let_ x $ if_ (var (v ZZ))
                             (weaken (and_ xs))
                             false

  -- Scheme returns the first true value
  or_ :: [lang 'Expr vs Bool] -> lang 'Expr vs Bool
  or_  []     = true
  -- We need to be careful in this to not repeat set's
  or_  (x:xs) = let_ x $ if_ (var (v ZZ))
                             true
                             (weaken (or_ xs))
