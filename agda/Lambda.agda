module Lambda where

  open import Lib

  module Base where
    -- Postulates about the language of types
    postulate
      ⋆ : Set
    -- and the fact that it has an arrow
      _⇒_ : ⋆ → ⋆ → ⋆

    ΛTy : Set
    ΛTy = ⋆
    Constraint : Set₁
    Constraint = Set

    ΛEnv : ℕ → Set
    ΛEnv = Vec ΛTy

    postulate
      known : ΛTy → Set

    -- Postulate the typing judgement
    data _⊢_ : {n : ℕ} → ΛEnv n → ΛTy → Set where
    -- postulate
    --  _⊢_ : {n : ℕ} → ΛEnv n → ΛTy → Set
    -- ... and its elements
      var : {n : ℕ} {env : ΛEnv n}
          → (p : Fin n)
          -- ----------
          → env ⊢ (env [ p ])
      app : {n : ℕ} {env : ΛEnv n} {a b : ΛTy}
          → env ⊢ (a ⇒ b) → env ⊢ a
          -- ------------------------
          → env ⊢ b
      lam : {n : ℕ} {env : ΛEnv n} {a b : ΛTy}
          → (a ∷ env) ⊢ b
          -- -------------
          → env ⊢ (a ⇒ b)
      kno : {n : ℕ} {env : ΛEnv n} {r : ΛTy}
          → known r
          -- -----------------------
          → env ⊢ r

  module Let where
    open Base public
    -- Typing rule and translation
    let! : {n : ℕ} {env : ΛEnv n} {a b : ΛTy}
         → env ⊢ a → (a ∷ env) ⊢ b
         -- -----------------------
         → env ⊢ b
    let! e₁ e₂ = app (lam e₂) e₁
    -- let! e₁ e₂ = app (lam e₁) e₂

  module Multilet where
    open Let public

    data LetList : {n m : ℕ} → ΛEnv n → Vec ΛTy m → ΛTy → Set where
      ■    : ∀ {n} {env : ΛEnv n} {r : ΛTy}
           → env ⊢ r
           -- ---------------
           → LetList env [] r
      _\\_ : ∀ {n m} {env : ΛEnv n}
               {x : ΛTy} {xs : Vec ΛTy m} {r : ΛTy}
           → env ⊢ x → LetList (x ∷ env) xs r
           -- --------------------------------
           → LetList env (x ∷ xs) r

    multilet : ∀ {n m} {env : ΛEnv n} {xs : Vec ΛTy m} {r : ΛTy}
             → LetList env xs r → env ⊢ r
    multilet (■ x)     = x
    multilet (l \\ ls) = let! l (multilet ls)

  module HasList where
    open Base public
    -- Postulate the type...
    postulate
      list : ⋆ → ⋆
      -- ... and the constructors
      nil  : ∀ {e} → known (list e)
      cons : ∀ {e} → known (e ⇒ (list e ⇒ list e))

  module HasBool where
    open Base public
    postulate
      bool : ⋆
      ifthenelse : ∀ {e} → known (bool ⇒ (e ⇒ (e ⇒ e)))

  module HasMap where
    open HasList public

    -- We need to postulate the type of map
    postulate
      map : ∀ {a b} → known ((a ⇒ b) ⇒ (list a ⇒ list b))

  module MapSpec where
    open HasMap public

    map_ : ∀ {n} {env : ΛEnv n} {fn lst a b e : ΛTy}
         → fn ≡ (a ⇒ b) → lst ≡ (list e) → a ≡ e
         → env ⊢ fn → env ⊢ lst
         -- -------------------------------------
         → env ⊢ list b
    map_ refl refl refl fn lst = app (app (kno map) fn) lst

  module HasFunctor where
    open HasList public

    postulate
      Functor : (⋆ → ⋆) → Constraint
      instance FunctorList : Functor list

      fmap : {f : ΛTy → ΛTy} {a b : ΛTy}
           → {{_ : Functor f}} → known ((a ⇒ b) ⇒ (f a ⇒ f b))

  module MapSpecFromFmap where
    open HasFunctor public

    map_ : ∀ {n} {env : ΛEnv n} {fn lst a b e : ΛTy}
         → fn ≡ (a ⇒ b) → lst ≡ (list e) → a ≡ e
         → env ⊢ fn → env ⊢ lst
         -- -------------------------------------
         → env ⊢ list b
    map_ refl refl refl fn lst = app (app (kno fmap) fn) lst

  module ListComprehensions where
    open Let
    open HasList
    open HasBool

    postulate
      concatMap : ∀ {a b : ΛTy} → known ((a ⇒ list b) ⇒ (list a ⇒ list b))

    data _⊢ℓ_ : {n : ℕ} → (env : ΛEnv n) → ΛTy → Set where
      -- [ e ]
      ■ : ∀ {n} {env : ΛEnv n} {r : ΛTy}
        → env ⊢ r
        -- --------
        → env ⊢ℓ r
      -- [ e | x <- lst ]
      pat : ∀ {n} {env : ΛEnv n} {a r : ΛTy}
          → env ⊢ (list a) → (a ∷ env) ⊢ℓ r
          -- -------------------------------
          → env ⊢ℓ r
      -- [ e | guard ]
      guard : ∀ {n} {env : ΛEnv n} {a r : ΛTy}
            → env ⊢ bool → env ⊢ℓ r
            -- ---------------------
            → env ⊢ℓ r
      -- [ e | let x = expr ]
      letc : ∀ {n} {env : ΛEnv n} {a r : ΛTy}
           → env ⊢ a → (a ∷ env) ⊢ℓ r
           -- -------------------------------
           → env ⊢ℓ r

    compr : ∀ {n} {env : ΛEnv n} {r}
          → env ⊢ℓ r → env ⊢ list r
    compr (■ x)       = app (app (kno cons) x) (kno nil)
    compr (pat x r)   = app (app (kno concatMap) (lam (compr r))) x
    compr (guard g r) = app (app (app (kno ifthenelse) g) (compr r)) (kno nil)
    compr (letc x r)  = let! x (compr r)

  module CompletenessOfMap where
    open Σ public
    open MapSpec public

    map-parser : Set
    map-parser = ∀ {n : ℕ} {env : ΛEnv n} {r : ΛTy}
                 → (expr : env ⊢ r)
                 → Maybe (Σ ({- a -} ΛTy × {- e -} ΛTy)  (λ tys →
                          Σ ((env ⊢ (projl tys ⇒ projr tys)) × (env ⊢ list (projl tys))) (λ terms →
                            ixeq r (list (projr tys)) expr (app (app (kno map) (projl terms)) (projr terms)) )))

    map-is-complete : ∀ {n} {env : ΛEnv n} {r : ΛTy}
                    → map-parser → env ⊢ r → env ⊢ r
    map-is-complete p e with p e
    map-is-complete p .(app (app (kno map) fn) lst) | Just ((a , b) , (fn , lst) , reflrefl)
                                              = map_ refl refl refl fn lst
    map-is-complete _ (var n)     | Nothing   = var n
    map-is-complete p (app e₁ e₂) | Nothing   = app (map-is-complete p e₁) (map-is-complete p e₂)
    map-is-complete p (lam b)     | Nothing   = lam (map-is-complete p b)
    map-is-complete _ (kno k)     | Nothing   = kno k
