{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GADTs #-}

{-# OPTIONS_GHC -Wall #-}

module Lib (
  Constraint
, Proxy(..)
, module Lib
) where

import GHC.Exts
import Data.Proxy

data Nat = Z | S Nat

-- Operations over lists

type family Ix (n :: Nat) xs where
  Ix 'Z     (x ': xs) = x
  Ix ('S n) (x ': xs) = Ix n xs

-- Technique: write predicates as type
-- families returning Constraint
type family Elem x ys :: Constraint where
  Elem x (x ': ys) = ()
  Elem x (y ': ys) = Elem x ys

type family Lookup x xs where
  Lookup k ('(k,v) ': zs) = v
  Lookup k (z ': zs)      = Lookup k zs

type family xs :++: ys where
  '[]       :++: ys = ys
  (x ': xs) :++: ys = x ': xs :++: ys

type family Remove (n :: Nat) xs where
  Remove 'Z     (x ': xs) = xs
  Remove ('S n) (x ': xs) = x ': Remove n xs

-- This gets stuck for (a ': vs) :--: vs
-- Because of the apartness check :(
type family zs :--: ys where
  zs        :--: zs = '[]
  (z ': zs) :--: ys = z ': (zs :--: ys)

data FVec f xs where
  FNil  :: FVec f '[]
  FCons :: f x -> FVec f xs -> FVec f (x ': xs)

data FNotVec (f :: k -> *) where
  FNotNil  :: FNotVec f
  FNotCons :: f x -> FNotVec f -> FNotVec f
